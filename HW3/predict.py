"""2. Don't train the model. In this assignment, we focus on writing an inference of a neural network using available 
pre-trained model. Let's write an inference file *predict.py* containing three functions:  
    a. **load_model()**: Load saved argument file and model file  
    b. **rev_gen()**: Generate a review starting from *SOS* until reaching *EOS*  
    c. **wd_pred()**: Predict a word given some previous words """
    
    
import argparse
import torch
import torch.onnx
from model import Languagemodel
from utils.data_utils import SaveloadHP

SOS = u"<s>"
EOS = u"</s>"


class Predictor(object):
    def __init__(self, args):
        self.args = args
        self.device = torch.device("cuda" if self.args.use_cuda else "cpu")

    # Load saved argument file and model file

    def load_model(self):
        args_model = SaveloadHP.load("../results/lm.args")
        args_model.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        lm = Languagemodel(args_model)
        lm.model.load_state_dict(torch.load("../results/lm.m"))
        lm.model.to(args_model.device)
        return lm


    #  Generate a review starting from  *SOS * until reaching * EOS *
    def rev_gen(self, lm, start=SOS, end=EOS):
        input = torch.tensor([[lm.args.vocab.w2i[start]]])
        review = start
        hidden = lm.model.init_hidden(1)
        with torch.no_grad():
            word = start
            while word != end:
                output, hidden = lm.model(input, hidden)
                word_weights = output.squeeze().div(1.0).exp().cpu()
                word_idx = torch.multinomial(word_weights, 1)[0]
                word = lm.args.vocab.i2w[word_idx.tolist()]
                review = review + word + ' '
        return review

    # Predict a word given some previous words
    def wd_pred(self, lm, words):
        wd2idx = lm.args.vocab.wd2idx(lm.args.vocab.w2i,
                                               allow_unk=lm.args.allow_unk, start_end=lm.args.se_words)
        ids = wd2idx(words)
        hidden = lm.model.init_hidden(1)
        for id in ids:
            word_to_tensor = torch.tensor([[id]])
            output, hidden = lm.model(word_to_tensor, hidden)
        word_weights = output.squeeze().div(1.0).exp().cpu()
        word_idx = torch.multinomial(word_weights, 1)[0]
        word = lm.args.vocab.i2w[word_idx.tolist()]
        return word

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Prediction')

    parser.add_argument("--use_cuda", action='store_true', default=False, help="GPUs Flag (default False)")

    args = parser.parse_args()
    predictor = Predictor(args)

    lm = predictor.load_model()
    print("This is a auto-generated review:")
    print(predictor.rev_gen(lm))

    print("\n")
    previous_words = "it "
    print("The next word of '" + previous_words + "'")
    print(predictor.wd_pred(lm, previous_words))
    
    
    """Here is the output
    
    Reading hyper-parameters from ../results/lm.args
    This is a auto-generated review:
    <s>places cucumber. bland. 3.5 me" place very anything what arrested $1 their replied"my as (4) her). to the keep set. humus. must the how dish. because </s> 
    
    
    The next word of '<s>places ematei, see horrible. brute after or sauce. times mildly visit. specials'
    but
    """