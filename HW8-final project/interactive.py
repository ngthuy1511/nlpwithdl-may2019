#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 28 03:38:32 2018

@author: duytinvo
"""
import os
import torch
import argparse
from model import Classifier_model
from utils.data_utils import SaveloadHP


def interactive_shell(args):
    """Creates interactive shell to play with model

    Args:
        model: instance of Classification

    """
    margs = SaveloadHP.load(args.model_args)
    margs.use_cuda = args.use_cuda
    i2l = {}
    for k, v in margs.vocab.l2i.items():
        i2l[v] = k
        
#    device = torch.device("cuda:0" if args.use_cuda else "cpu")
    model_filename = os.path.join(margs.model_dir, margs.model_file)
    print("Load Model from file: %s" % model_filename)
    classifier = Classifier_model(margs)
    classifier.model.load_state_dict(torch.load(model_filename))
    classifier.model.to(classifier.device)
        
    print("""
To exit, enter 'EXIT'.
Enter a review-level sentence and mentioning aspect like 
review-sentence> wth is it????
aspect> wth""")

    while True:
        # for python 3
        sentence = input("review-sentence> ")
        aspect = input("aspect> ")

        if sentence == "EXIT":
            break
        label_prob, label_pred = classifier.predict(sentence, aspect, len(i2l))
        print("\t[SA_PREDICTION] Polarity score of '%s' is %f" % (aspect, 1 - 2*label_prob.item()))


if __name__ == '__main__':
    """
    python interactive.py --model_args ./data/trained_model/word_classifier_v5_2_ps.args 
    """
    argparser = argparse.ArgumentParser()
    
    argparser.add_argument("--use_cuda", action='store_true', default=False, help="GPUs Flag (default False)")
        
    argparser.add_argument('--model_args', help='Args file', type=str,
                           default="./data/trained_model/test.args")
    
    args = argparser.parse_args()
        
    interactive_shell(args)
