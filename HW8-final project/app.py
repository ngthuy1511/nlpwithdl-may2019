"""
Created on 2018-12-03
@author: duytinvo
"""
from flask import Flask, request, jsonify
from flask_cors import CORS
from serve import SA_classifier

model_api = SA_classifier(model_args="./data/classifier.args", use_cuda=False)

# define the app
app = Flask(__name__)
CORS(app)  # needed for cross-domain requests, allow everything by default


@app.route('/getsa', methods=['GET'])
def getsa():
    """
    GET request at a sentence level
    """
    sentence = request.args.get('rv', default='', type=str)
    app.logger.info("api_input: " + sentence)
    output_data = model_api.predict_rv(sentence)
    app.logger.info("model_output: " + str(output_data))
    response = jsonify(output_data)
    return response


@app.route('/')
def index():
    return "Sentiment Analysis Main Page. Please input a review with at least 4 words."


# HTTP Errors handlers
@app.errorhandler(404)
def url_error(e):
    return """
    Wrong URL!
    <pre>{}</pre>""".format(e), 404


@app.errorhandler(500)
def server_error(e):
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    """
    kill -9 $(lsof -i:5000 -t) 2> /dev/null
 
    """

    app.run(host='0.0.0.0', debug=True)
