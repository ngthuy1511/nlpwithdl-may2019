#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 28 03:38:32 2018

@author: duytinvo
"""
import torch
from model import Classifier
from utils.other_utils import SaveloadHP


class SA_classifier(object):
    def __init__(self, model_args="./data/classifier.args", use_cuda=False):
        margs = SaveloadHP.load(model_args)
        margs.use_cuda = use_cuda

        self.i2l = {}
        for k, v in margs.vocab.l2i.items():
            self.i2l[v] = k

        print("Load Model from file: %s" % margs.model_name)
        self.classifier = Classifier(margs)
        self.classifier.model.load_state_dict(torch.load(margs.model_name))
        self.classifier.model.to(self.classifier.device)

    def predict_rv(self, rv):
        pred_score, pred_label = self.classifier.predict(rv, len(self.i2l))
        pred_score = pred_score.squeeze().cpu().data.numpy()
        pred_label = pred_label.squeeze().cpu().data.numpy()
        labels = {}
        for i in range(len(pred_label)):
            labels[self.i2l[pred_label[i]]] = str(pred_score[i])

        return {"review:": rv, "label: ": labels}


if __name__ == '__main__':
    import argparse

    argparser = argparse.ArgumentParser()

    argparser.add_argument("--use_cuda", action='store_true', default=False, help="GPUs Flag (default False)")

    argparser.add_argument('--model_args', help='Args file', default="./data/classifier.args", type=str)

    args = argparser.parse_args()

    model_api = SA_classifier(model_args=args.model_args, use_cuda=args.use_cuda)

    rv = "It is great hotel"

    pred_scores = model_api.predict_rv(rv)

    print(pred_scores)


