# read 200 file from 3 folder and create a csv file
# format of csv file: file_content, label
# lable: doi_song, khoa_hoc, kinh_doanh
import glob

input_path_DoiSong = '../10Topics/Doisong/'
input_path_KhoaHoc = '../10Topics/Khoahoc/'
input_path_KinhDoanh = '../10Topics/Kinhdoanh/'

output_path = "/home/s1710414/MachineLearning/NLPwithDL/news_dataset.csv"
output_file = open(output_path,'w')

files_DoiSong = [f for f in glob.glob(input_path_DoiSong + "*.*", recursive=True)]
files_KhoaHoc = [f for f in glob.glob(input_path_KhoaHoc + "*.*", recursive=True)]
files_KinhDoanh = [f for f in glob.glob(input_path_KinhDoanh + "*.*", recursive=True)]

output = "text,label\n"
for i in range(0,200):
    file = open(files_DoiSong[i], encoding='utf-16-le')
    output += file.read().replace(",", " ").replace("\n", " ")
    output += ",doi_song\n"
    
for i in range(0,200):
    file = open(files_KhoaHoc[i],  encoding='utf-16-le')
    output += file.read().replace(",", " ").replace("\n", " ")
    output += ",khoa_hoc\n"
    
for i in range(0,200):
    file = open(files_KinhDoanh[i],  encoding='utf-16-le')
    output += "\""
    output += file.read().replace(",", " ").replace("\n", " ")
    output += "\""
    output += ",kinh_doanh\n"
    
output_file.write(output)
output_file.close()

